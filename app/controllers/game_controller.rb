class GameController < ApplicationController
  before_filter :get_board, :only => [:play, :view, :check, :join, :consider, :stop, :update, :message]
  layout 'board', :only => [:play, :view]

  def get_board
    @board = Board.find_by_id(params[:id], :include => :checks)
    @last_check = @board.checks.last
  end

  def current_user
    @board.turn > 0 ? @board.player1 : @board.player2
  end

  def other_user
    @board.turn > 0 ? @board.player2 : @board.player1
  end

  def index
    redirect_to :action => :list
  end

  def play
    player = current_user
  end


  def check
    player = current_user
    raise "Not your turn" unless session[@board.id] == player
    x = params[:x].to_i
    y = params[:y].to_i
    raise "Invalid data" unless x >= 0 && x < @board.size && y >= 0 && y < @board.size && (!@board.member?(x,y))
    @board.check!(x,y)
    if request.xml_http_request?
      @board.checks.reload
      @from = @board.checks.size
      js = render_to_string :update do |page|
        page.replace_html "c#{x}#{y}", html_check(@board, x, y)
        page.visual_effect :highlight, "c#{x}#{y}" ,
                           :startcolor => ( session[@board.id] == @board.player1 ? '#ff9999' : '#9999ff' )
      end
      Meteor.shoot 'board', js
      render :nothing => true
    else
      redirect_to :action => :play, :id => params[:id]
    end
  rescue Exception => e
    render(:update) do |page|
      page.replace_html :message, e.message
      page.show :message_container
    end
  end

  def new
    @board = Board.new
    @board.size = 16
  end

  def create
    @board = Board.new
    @board.player1 = params[:player]
    @board.ip1 = request.remote_ip
    @board.player2 = ''
    @board.ip2 = ''
    @board.size = params[:size]
    @board.turn = 0
    @board.save!
    session[@board.id] = @board.player1
    redirect_to :action => :list
  end

  def list
    @boards = Board.find :all, params[:completed].nil? ? 'winner IS NULL' : 'winner IS NOT NULL'
  end

  def join
    raise "You have already joined this game" unless session[@board.id].nil?
    @board.player2 = params[:player]
    @board.ip2 = request.remote_ip
    @board.save!
    session[@board.id] = @board.player2
    redirect_to :action => :play, :id => @board.id
  end

  def stop
    @board.winner = session[@board.id] == @board.player1 ? @board.player2 : @board.player1
    @board.save!
    redirect_to :action => :list
  end

  def message
    m = @board.messages.build;
    m.message = "#{session[@board.id] || "Other"} said '#{params[:message]}'"
    m.save!
    if request.xml_http_request?
      js = render :update do |page|
        page.replace_html :message, m.message
        page.show :message_container
      end
      Meteor.shoot 'board', js
      render :nothing => true
    else
      if session[@board.id].nil?
        redirect_to :action => :view, :id => @board.id
      else
        redirect_to :action => :play, :id => @board.id
      end
    end
  end
end
