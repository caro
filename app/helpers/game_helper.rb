module GameHelper
  def html_check(board,x,y,active = true)
    c = board.member?(x,y)
    unless c.nil?
      c.turn > 0 ? '<span class="move1">X</span>' : '<span class="move2">O</span>'
    else
      '&nbsp;'
    end
  end
end
