class Board < ActiveRecord::Base
  has_many :checks, :order => "created_at ASC"
  has_many :messages, :order => "created_at ASC"

  def member?(x,y)
    return nil if self.new_record?
    self.checks.find_by_x_and_y(x,y)
  end

  def check!(x,y)
    c = Check.new
    c.board_id = self.id
    c.x = x
    c.y = y
    c.turn = self.turn
    c.save!
    self.turn = 1 - self.turn
    self.save!
  end
end
