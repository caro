class CreateBoards < ActiveRecord::Migration
  def self.up
    create_table :boards do |t|
      # t.column :name, :string
      t.column :data, :text, :null => false
      t.column :player1, :string, :null => false
      t.column :ip1, :string, :null => false
      t.column :player2, :string, :null => false
      t.column :ip2, :string, :null => false
      t.column :size, :integer, :null => false
      t.column :turn, :integer, :null => false
    end
  end

  def self.down
    drop_table :boards
  end
end
