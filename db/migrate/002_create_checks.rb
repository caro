class CreateChecks < ActiveRecord::Migration
  def self.up
    create_table :checks do |t|
      # t.column :name, :string
      t.column :board_id, :integer, :null => false
      t.column :created_at, :timestamp, :null => false
      t.column :turn, :integer, :null => false
      t.column :x, :integer, :null => false
      t.column :y, :integer, :null => false
    end
  end

  def self.down
    drop_table :checks
  end
end
