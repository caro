class AddWinnerInBoard < ActiveRecord::Migration
  def self.up
    add_column :boards, :winner, :string
  end

  def self.down
    remove_column :boards, :winner
  end
end
