class CreateMessages < ActiveRecord::Migration
  def self.up
    create_table :messages do |t|
      # t.column :name, :string
      t.column :board_id, :integer, :null => false
      t.column :created_at, :timestamp, :null => false
      t.column :message, :string, :null => false
    end
  end

  def self.down
    drop_table :messages
  end
end
