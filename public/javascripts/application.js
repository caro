function failure(request)
{
	$("message").update("HTTP Error status " + request.status + "!");
	$("message_container").show();
}

function set_move(i,j,turn)
{
	id = "c" + i + j;
	mark = turn > 0 ? '<span class="move1">X</span>' : '<span class="move2">O</span>';
	Element.update(id, mark);
	Element.removeClassName(id,'empty_cell');
	new Effect.Highlight(id, {});
}

function update_state(state,last)
{
	client_state = state;
	last_id = last;
}

function say(msg)
{
	Element.update('message', msg);
	new Effect.Appear('message_container');
	chat_state ++;
	setTimeout("new Effect.Fade('message_container')",5000);
}

function send_message(frm,url)
{
	new Ajax.Request(url,
		{parameters: Form.serialize(frm),
			asynchronous:true,
			evalScripts:true});
	Form.reset(frm);
}
